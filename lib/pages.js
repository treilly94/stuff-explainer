import fs from 'fs'
import path from 'path'

const pagesDirectory = path.join(process.cwd(), 'pages')

export function getSortedPages() {
    const allFiles = []
    const directories = fs.readdirSync(pagesDirectory, { withFileTypes: true })
        .filter(dir => dir.isDirectory())
        .map(dir => dir.name)

    for (let dir of directories) {
        const dirFullPath = path.join(pagesDirectory, dir)
        const files = fs.readdirSync(dirFullPath).map(fileName => {
            return dir.concat('/' + fileName.replace(/\.js$/, ''))
        })
        allFiles.push(...files)
    }

    return allFiles.sort()
}
