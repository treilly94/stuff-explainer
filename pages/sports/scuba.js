import Head from 'next/head'
import Link from 'next/link'
import Layout from '../../components/layout'

export default function SCUBA() {
    return (
        <Layout>
            <Head>
                <title>scuba</title>
            </Head>
            <h1>SCUBA Diving</h1>
            <h2>
                <Link href="/">
                    <a>Back to home</a>
                </Link>
            </h2>
        </Layout>
    )
}
