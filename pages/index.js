import Layout from "../components/layout"
import ListOfLinks from "../components/list-of-links"
import { getSortedPages } from '../lib/pages'

export async function getStaticProps() {
  const allPages = getSortedPages()
  return {
    props: {
      allPages: allPages
    }
  }
}

export default function Home(allPages) {
  return (
    <Layout>
      <ListOfLinks {...allPages} />
    </Layout>
  )
}
