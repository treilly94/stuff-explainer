import Head from 'next/head'
import NavBar from './navbar'

export default function Layout({ children }) {
    return (
        <div>
            <Head>
                <meta name="og:title" content='Stuff Explainer' />
                <meta
                    name="description"
                    content="A place where I describe random stuff"
                />
            </Head>
            <NavBar />
            <main>
                {children}
            </main>
        </div>
    )
}
