import Link from 'next/link'

import styles from './navbar.module.css'

export default function NavBar(home) {
    return (
        <nav className={styles.navbar}>
            <Link href="/" passHref>
                <h1>Stuff Explainer</h1>
            </Link>
        </nav>
    )
}
