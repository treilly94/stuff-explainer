import Link from "next/link";
import styles from './list-of-links.module.css'

export default function ListOfLinks(props) {
    return (
        <div className={styles.container}>
            <ul>
                {
                    props.allPages.map(page =>
                        <Link key={page} href={`/${page}`} passHref>
                            <li>
                                <a>{page}</a>
                            </li>
                        </Link>
                    )
                }
            </ul>
        </div>
    )
}
